import { createApp,h } from 'vue'
import App from './App.vue'
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import '@splidejs/splide/dist/css/themes/splide-sea-green.min.css';
import '@splidejs/splide/dist/css/themes/splide-skyblue.min.css';

import './index.css'




const app  = createApp({
    render: ()=>h(App)
});

app.mount("#app")

// createApp(App).mount('#app')
